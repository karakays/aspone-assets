$(document).ready(function() {
  var config = {
    admin: {
      apiUri: "/api/assets",
      columns: [
        { data: "id", visible: false },
        { data: "symbol", title: "Symbol" },
        {
          data: "rate",
          title: "Rate",
          render: function(data, type, row) {
              return data.currency + ' ' + data.value;
          }	
        },
        {
          data: "spread",
          title: "Spread",
          render: function(data, type, row) {
            return "<input type='text' value='" + data + "'/>";
          }
        }
      ]
    },
    dashboard: {
      apiUri: "/api/prices",
      columns: [
        { data: "symbol", title: "Symbol" },
        { data: "bid", title: "Bid",
        	render: function(data, type, row) {
        		return data.currency + ' ' + data.value;
            }	
        },
        { data: "offer", title: "Offer",
        	render: function(data, type, row) {
        		return data.currency + ' ' + data.value;
            }	
        },
        {
          data: null,
          defaultContent: "<button class='btn btn-dark'>Take</button>"
        }
      ]
    }
  };

  function activePage() {
    var pathArr = location.href.split("/");
    return pathArr[pathArr.length - 1];
  }

  function getConfig(param) {
    var index = activePage();

    return config[index][param];
  }

  var table = $("#example").DataTable({
    ajax: getConfig("apiUri"),
    searching: false,
    ordering: false,
    paging: false,
    info: false,
    columns: getConfig("columns")
  });

  $("#example tbody").on("click", "button", function() {
    var data = table.row($(this).parents("tr")).data();
    console.log(data);

    $.ajax({
      method: "POST",
      dataType: "json",
      url: "/api/trades/",
      headers: {
    	 "Content-Type": "application/json" 
      },
      data: JSON.stringify({ assetId: data.id })
    }).done(function(msg) {
      console.log("Data Saved: " + msg);
    });
  });

  $("#example tbody").on("change", "input", function() {
    var input = $(this);
    var data = table.row(input.parents("tr")).data();

    $.ajax({
      method: "PUT",
      dataType: "json",
      url: "/api/assets/" + data.id,
      headers: {
     	 "Content-Type": "application/json" 
       },
      data: JSON.stringify({ spread: input.val()})
    }).done(function(msg) {
      console.log("Data Saved: " + msg.data);
    });

    console.log(data);
  });
});
