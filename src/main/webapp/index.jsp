<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="./assets/vendor/css/bootstrap.min.css">
    <link rel="stylesheet" href="./assets/vendor/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="./assets/main.css">
</head>

<body>
    <div class="container">
        <div class="row justify-content-md-center">
            <div class="col-md-7">
                <table id="example" class="display" style="width:100%">
                    <thead></thead>
                    <tbody></tbody>
                </table>
            </div>
        </div>
    </div>

    <script src="./assets/vendor/js/jquery-3.3.1.min.js"></script>
    <script src="./assets/vendor/js/bootstrap.bundle.min.js"></script>
    <script src="./assets/vendor/js/jquery.dataTables.min.js"></script>
    <script src="./assets/main.js"></script>
</body>

</html>