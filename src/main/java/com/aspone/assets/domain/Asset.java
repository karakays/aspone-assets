package com.aspone.assets.domain;

import java.math.BigDecimal;
import java.util.Date;

import javax.money.MonetaryAmount;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

import org.hibernate.annotations.Columns;
import org.javamoney.moneta.Money;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Represents an asset, uniquely identified by <code>this.symbol</code>
 * 
 * * @author Selçuk Karakayalı
 */
@Data
@Entity
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"symbol"})})
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Asset {
    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date createdDate;
    
    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = true)
    private Date modifiedDate;
   
    @Column(nullable = false)
    private String symbol;
    
    @Columns(columns = {@Column(name = "currency"), @Column(name = "amount")})
    @org.hibernate.annotations.Type(type = "org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency")
    private Money price;
    
    @Column
    private BigDecimal spread;

    public Asset(String symbol, MonetaryAmount price, BigDecimal spread) {
        this.symbol = symbol;
        this.price = Money.from(price);
        this.spread = spread;
        this.createdDate = new Date();
    }
    
    public String toString() {
        return String.format("Asset [id=%s, symbol=%s, price=%s, spread=%s]",
                id, symbol, spread);
    }
}
