package com.aspone.assets.domain;

import java.util.Date;

import javax.money.MonetaryAmount;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Columns;
import org.javamoney.moneta.Money;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

/**
 * Represents a trade operation of an {@link Asset}, <code>this.asset</code>
 * 
 * * @author Selçuk Karakayalı
 */
@Data
@Entity
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor
@EqualsAndHashCode(of = {"id"})
public class Trade {
    @Id
    @Column(nullable = false, updatable = false)
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date createdDate;
   
    @Columns(columns = {@Column(name = "currency"), @Column(name = "amount")})
    @org.hibernate.annotations.Type(type = "org.jadira.usertype.moneyandcurrency.moneta.PersistentMoneyAmountAndCurrency")
    private Money price;
    
    @ManyToOne
    @JoinColumn(name = "asset_id", nullable = false)
    private Asset asset;

    public Trade(MonetaryAmount price, Asset asset) {
        this.price = Money.from(price);
        this.asset = asset;
        this.createdDate = new Date();
    }
    
    public String toString() {
        return String.format("Trade [id=%s, price=%s, asset=%s]",
                id, price, asset.getId());
    }
}
