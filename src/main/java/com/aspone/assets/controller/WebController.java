package com.aspone.assets.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/**
 * Controller that handles operations related to views only,
 * e.g. rendering a view, redirecting to a view etc.
 *  
 * @author Selçuk Karakayalı
 */
@Controller
public class WebController {
    
    @GetMapping(value = "/")
    public String index() {
        return "redirect:/dashboard";
    }
    
    @GetMapping(value = "/dashboard")
    public String dashboard() {
        return "index";
    }
    
    @GetMapping(value = "/admin")
    public String admin() {
        return "index";
    }
    
    @GetMapping(value = "/fail")
    public String fail() {
        throw new RuntimeException("serious error");
    }
}
