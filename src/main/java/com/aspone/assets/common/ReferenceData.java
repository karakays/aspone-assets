package com.aspone.assets.common;

import java.util.List;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Component
@Getter
@Setter
@ConfigurationProperties(prefix = "referenceData")
public class ReferenceData {
    private List<AssetReferenceData> symbols;
}
