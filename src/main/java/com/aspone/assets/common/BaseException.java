package com.aspone.assets.common;

import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Base error type of the API.
 * 
 * @author karakays
 */
@Getter
@EqualsAndHashCode(callSuper = true)
public class BaseException extends RuntimeException {
    private static final long serialVersionUID = 1886152412399264981L;
    private final Object details;
    private final int errorCode;

    public BaseException(int errorCode, String message, Object details) {
        super(message);
        this.errorCode = errorCode;
        this.details = details;
    }

    public BaseException(int errorCode, String message) {
        this(errorCode, message, null);
    }
}
