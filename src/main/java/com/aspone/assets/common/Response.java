package com.aspone.assets.common;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;
import lombok.Getter;

/**
 * Standard response of the API.
 * 
 * @param <T> subject of this.response
 * 
 * @author Selçuk Karakayalı
 */
@Getter
public class Response<T> implements Serializable {
    private static final long serialVersionUID = -6905912337834196207L;
    private final T data;
    private final Error error;
    
    private Response(T data, Error error) {
        this.data = data;
        this.error = error;
    }
    
    public Response(T data) {
        this(data, null);
    }

    public Response(Error error) {
        this(null, error);
    }

    @Data
    @JsonIgnoreProperties(ignoreUnknown = true)
    public static class Error implements Serializable {
        private static final long serialVersionUID = -3357905591665674360L;

        private final String code;

        private final String description;
    }
}
