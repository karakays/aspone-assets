package com.aspone.assets.common;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class AssetReferenceData {
    private String symbol;
    private BigDecimal spread;
}
