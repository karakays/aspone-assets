package com.aspone.assets.common;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@ControllerAdvice(basePackages = "com.aspone.assets.api")
public class ApiExceptionHandler  {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<Object> handleExceptionInternal(Exception ex) {
       log.error("Very serious error, here", ex);
       return new ResponseEntity<Object>("Very serious error, here", HttpStatus.INTERNAL_SERVER_ERROR);
   }
    
    @ExceptionHandler(BaseException.class)
    public ResponseEntity<Object> handleBaseException(BaseException ex) {
        log.error("BaseException occurred", ex);
        Response.Error error = new Response.Error(ex.getErrorCode() + "", ex.getMessage()); 
        return new ResponseEntity<>(new Response<Void>(error), HttpStatus.valueOf(ex.getErrorCode()));
   }
}
