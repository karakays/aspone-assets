package com.aspone.assets.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.aspone.assets.domain.Asset;
import com.aspone.assets.domain.Trade;

public interface AssetService {
    List<Asset> load();
    Optional<Asset> loadById(Long assetId);
    Optional<Asset> loadBySymbol(String symbol);
    Asset create(Asset asset);
    List<Asset> create(List<Asset> assets);
    Asset updateSpread(Long assetId, BigDecimal spread);
    Trade createTrade(Long assetId);
}
