package com.aspone.assets.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.javamoney.moneta.Money;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.aspone.assets.common.BaseException;
import com.aspone.assets.common.ReferenceData;
import com.aspone.assets.domain.Asset;
import com.aspone.assets.domain.Trade;
import com.aspone.assets.repository.AssetRepository;
import com.aspone.assets.repository.TradeRepository;
import com.aspone.assets.service.AssetService;

/**
 * Provides main business service for {@link} Asset.
 * 
 * @author karakays
 */
@Service
public class AssetServiceImpl implements AssetService {
    
    private final AssetRepository repository;
    private final TradeRepository tradeRepository;
    private final ReferenceData referenceData;
    
    public AssetServiceImpl(AssetRepository repository, TradeRepository tradeRepository, ReferenceData data) {
        this.repository = repository;
        this.tradeRepository = tradeRepository;
        this.referenceData = data;
    }
    
    // Initialize assets
    @PostConstruct
    void init() {
        List<Asset> assets = referenceData.getSymbols().stream()
                .map(symbol -> new Asset(symbol.getSymbol(),
                        Money.of(Math.random() * 1000, "USD"), symbol.getSpread()))
                .collect(Collectors.toList());
        
        create(assets);
    }

    @Override
    @Transactional(readOnly = false)
    public Asset create(Asset asset) {
        Optional<Asset> optional = repository.findBySymbol(asset.getSymbol());
        if(optional.isPresent()) {
            throw new BaseException(1001, "Asset already exists.", asset.getSymbol());
        }
        return repository.save(asset);
    }

    @Override
    public Optional<Asset> loadBySymbol(String symbol) {
        return repository.findBySymbol(symbol);
    }

    @Override
    public Optional<Asset> loadById(Long assetId) {
        return Optional.ofNullable(repository.findOne(assetId));
    }

    @Override
    @Transactional(readOnly = false)
    public List<Asset> create(List<Asset> contacts) {
        List<Asset> result = new ArrayList<>();
        contacts.forEach((a) -> result.add(create(a)));
        return result;
    }

    @Override
    public List<Asset> load() {
        return repository.findAll();
    }
    
    @Override
    @Transactional(readOnly = false)
    public Asset updateSpread(Long assetId, BigDecimal spread) {
        Asset asset = loadById(assetId)
                .orElseThrow(() -> new BaseException(404, "Asset not found."));

        asset.setSpread(spread);
        asset.setModifiedDate(new Date());
        return asset;
    }

    @Override
    @Transactional(readOnly = false)
    public Trade createTrade(Long assetId) {
        Asset asset = loadById(assetId)
                .orElseThrow(() -> new BaseException(404, "Asset not found."));
        
        Trade trade = new Trade(asset.getPrice(), asset);
        return tradeRepository.save(trade);
    }
}
