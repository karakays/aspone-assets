package com.aspone.assets.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.zalando.jackson.datatype.money.FieldNames;
import org.zalando.jackson.datatype.money.MoneyModule;

import com.fasterxml.jackson.databind.Module;

@Configuration
public class JsonConfig {
    
    // Custom serializer module for javax.money.MonetaryAmount
    @Bean
    public Module moneyModule() {
        return new MoneyModule().withFieldNames(FieldNames.valueOf("value", "currency", "formatted"));
    }
}
