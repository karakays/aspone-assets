package com.aspone.assets.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aspone.assets.domain.Trade;

@Repository
public interface TradeRepository extends JpaRepository<Trade, Long> {

}