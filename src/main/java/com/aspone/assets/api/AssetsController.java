package com.aspone.assets.api;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.aspone.assets.api.model.AssetDto;
import com.aspone.assets.api.model.AssetPriceDto;
import com.aspone.assets.api.model.CreateTradeDto;
import com.aspone.assets.api.model.UpdateAssetDto;
import com.aspone.assets.common.BaseException;
import com.aspone.assets.common.Response;
import com.aspone.assets.domain.Asset;
import com.aspone.assets.service.AssetService;

/**
 * Main interface of the application.
 * For response format, @see {@link Response}
 *  
 * @author Selçuk Karakayalı
 */
@RestController
public class AssetsController {
    
    private final AssetService assetService;
    
    public AssetsController(AssetService assetService) {
        this.assetService = assetService;
    }
    
    @GetMapping(value = "/api/assets")
    public List<AssetDto> getAssets() {
        List<Asset> assets = assetService.load();
        return map(assets);
    }
    
    @GetMapping(value = "/api/assets/{assetId}")
    public AssetDto getAssetById(@PathVariable Long assetId) {
        Optional<Asset> asset = assetService.loadById(assetId);
        if(!asset.isPresent()) {
            throw new BaseException(404, "Asset not found.");
        }
        return map(asset.get());
    }
    
    @GetMapping(value = "/api/assets", params = "symbol")
    public AssetDto getAssetBySymbol(@RequestParam(required = true) String symbol) {
        Optional<Asset> asset = assetService.loadBySymbol(symbol);
        if(!asset.isPresent()) {
            throw new BaseException(404, "Asset not found.");
        }
        return map(asset.get());
    }
    
    @PutMapping(value = "/api/assets/{assetId}")
    public AssetDto updateAsset(@PathVariable Long assetId, @Valid @RequestBody UpdateAssetDto updateAssetDto) {
        Asset asset = assetService.updateSpread(assetId, updateAssetDto.getSpread());
        return map(asset);
    }
    
    @GetMapping(value = "/api/prices")
    public List<AssetPriceDto> getPrices() {
        List<Asset> assets = assetService.load();
        return mapPrice(assets);
    }
    
    @PostMapping(value = "/api/trades")
    public ResponseEntity<Void> createTrade(@Valid @RequestBody CreateTradeDto createTradeDto) {
        assetService.createTrade(createTradeDto.getAssetId());
        return ResponseEntity.noContent().build();
    }
    
    private AssetDto map(Asset asset) {
        return new AssetDto(asset.getId(), asset.getCreatedDate(), asset.getModifiedDate(),
                asset.getSymbol(), asset.getPrice(), asset.getSpread());
    }
    
    private List<AssetDto> map(List<Asset> assets) {
        return assets.stream().map(a -> map(a)).collect(Collectors.toList());
    }
    
    private AssetPriceDto mapPrice(Asset asset) {
        return new AssetPriceDto(asset);
    }
    
    private List<AssetPriceDto> mapPrice(List<Asset> assets) {
        return assets.stream().map(a -> mapPrice(a)).collect(Collectors.toList());
    }
}
