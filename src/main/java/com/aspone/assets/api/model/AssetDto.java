package com.aspone.assets.api.model;

import java.math.BigDecimal;
import java.util.Date;

import javax.money.MonetaryAmount;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Represents transfer object of the {@link} Asset domain object.
 * 
 * @author Selçuk Karakayalı
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode(of = "symbol")
public class AssetDto {
    private final Long id;
    private final Date createdDate;
    private final Date modifiedDate;
    private final String symbol;
    private final MonetaryAmount rate;
    private final BigDecimal spread;
}
