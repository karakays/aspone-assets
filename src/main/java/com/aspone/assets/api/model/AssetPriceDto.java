package com.aspone.assets.api.model;

import java.math.BigDecimal;

import javax.money.MonetaryAmount;

import org.javamoney.moneta.Money;

import com.aspone.assets.domain.Asset;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

/**
 * Represents transfer object of bid/offer prices of an {@link} Asset.
 * Prices are calculated based on spread.
 * 
 * @author Selçuk Karakayalı
 */
@Getter
@AllArgsConstructor
@EqualsAndHashCode(of = "symbol")
public class AssetPriceDto {
    private final Long id;
    private final String symbol;
    private final MonetaryAmount bid;
    private final MonetaryAmount offer;
    
    public AssetPriceDto(Asset asset) {
        this.id = asset.getId();
        this.symbol = asset.getSymbol();
        BigDecimal offset = asset.getSpread().multiply(BigDecimal.valueOf(0.5));
        MonetaryAmount offsetAmount = Money.of(offset, asset.getPrice().getCurrency());
        this.bid = asset.getPrice().subtract(offsetAmount);
        this.offer = asset.getPrice().add(offsetAmount);
    }
}
