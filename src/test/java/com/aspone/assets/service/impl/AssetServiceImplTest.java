package com.aspone.assets.service.impl;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.Optional;

import org.javamoney.moneta.Money;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.aspone.assets.common.BaseException;
import com.aspone.assets.common.ReferenceData;
import com.aspone.assets.domain.Asset;
import com.aspone.assets.repository.AssetRepository;
import com.aspone.assets.repository.TradeRepository;
import com.aspone.assets.service.AssetService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AssetServiceImplTest {
    
    @Autowired
    private AssetService assetService;
    
    @MockBean
    private TradeRepository tradeRepository;
    
    @MockBean
    private ReferenceData referenceData;

    @MockBean
    private AssetRepository repository;
    
    private final Money TEN_USD = Money.of(BigDecimal.TEN, "USD");
    
    @Test
    public void shouldReturnAssetgivenAssetWhenExisting(){
        Asset asset = new Asset("ABC", TEN_USD, BigDecimal.ONE);
        asset.setId(1L);
                
        Mockito.when(repository.findOne(1L))
          .thenReturn(asset);
        
        Optional<Asset> assetOptional = assetService.loadById(1L);
        
        assertTrue(assetOptional.isPresent() && assetOptional.get().getSymbol().equals("ABC"));
    }
    
    @Test
    public void givenAssetWhenExistingSymbolThenThrowException(){
        Asset asset = new Asset("ABC", TEN_USD, BigDecimal.ONE);
        asset.setId(1L);
                
        Mockito.when(repository.findBySymbol("ABC"))
          .thenReturn(Optional.of(asset));
                
        try {
            assetService.create(asset);
        } catch(BaseException ex) {
            assertTrue(ex.getErrorCode() == 1001);
        }        
    }
    
    @Test
    public void shouldUpdateSpreadgivenAssetWhenUpdatingSpread(){
        Asset asset = new Asset("ABC", TEN_USD, BigDecimal.ONE);
        asset.setId(1L);
                
        Mockito.when(repository.findOne(1L))
          .thenReturn(asset);
        
        assetService.updateSpread(1L, BigDecimal.TEN);
        
        assertTrue((asset.getSpread().equals(BigDecimal.TEN) && asset.getModifiedDate() != null));
    }
    
    @Test
    public void shouldReturnErrorgivenNonExistingAssetWhenUpdatingSpread(){
        Asset asset = new Asset("ABC", TEN_USD, BigDecimal.ONE);
        asset.setId(1L);
                
        Mockito.when(repository.findOne(1L))
          .thenReturn(asset);
        
        try {
            assetService.updateSpread(2L, BigDecimal.TEN);
        } catch(BaseException ex) {
            assertTrue(ex.getErrorCode() == 404);
        }
    }
}
