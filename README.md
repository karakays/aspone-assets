

![](http://aspone.co.uk/wp-content/uploads/2016/08/logo.png)


ASPone challenge: assets
============


## Getting started

### Prerequisites

 - JRE 1.8 or higher
 - Maven build tool


### Cloning Repo
You can clone the repository as below. Before that, just make sure I got your public ssh key added into the access keys.

```
    $ git clone git@bitbucket.org:karakays/aspone-assets.git
    $ cd aspone-assets/
```

As an alternative, you can extract it from the tarball if you have the archive.

```
    $ tar xzvf aspone-assets.tar.gz
    $ cd aspone-assets/
``` 
    
### Build
Get a build using maven which results with an artifact of a WAR package in the /target directory.

```
	$ mvn clean package
```

### Deployment
Application uses embedded servlet container (Tomcat) and embedded in-memory database (H2). This makes the deployment almost configuration-free.

You can boot the application using the java command. API PORT is 8000, by default. If necessary, you can override it by using environment variable as shown below.

```bash
	$ export API_PORT=8888		# override API_PORT
	$ java -jar target/assets-0.0.1-SNAPSHOT.war
```

You can access the dashboard from the URL `http://localhost:8000` and you'll get redirected to trader screen at `/dashboard`. The second broker screen is hosted at `/admin` page.


  

## Highlights

### API

Application has a RESTful API with the content-type of JSON. I tried to follow REST conventions where HTTP methods are used to describe the action itself.

| HTTP method 	| Path        		| Desription  									|
| ------------- |:------------		|:-------------:								|
| GET      		| /assets 			| 	Retrieve all resources.						|
| GET 			| /assets/{id}		|   Retrieve a particular resource with {id} 	|
| GET 			| /assets?key=value	|   Query resources with key-value pairs 		|
| PUT 			| /assets/{id}		|   Update resource with {id}. 					|
| POST			| /trades			|  	Used to create a new resource		 		|
| ... 			| ...				|  	...									 		|

To test the API, you can use any HTTP client, for instance

```bash
curl -X GET http://localhost:8000/api/assets
```

You can retrieve a particular asset by its id

```bash
curl -X GET http://localhost:8000/api/assets/1
```

or query it by its symbol

```bash
curl -X GET http://localhost:8000/api/assets?symbol=AAL.L
```

```bash
curl -X GET http://localhost:8000/api/prices
```

```bash
curl -X POST \
  http://localhost:8000/api/trades \
  -H 'content-type: application/json' \
  -d '{
      "assetId": "1"
	}'
```

API response has a standard format that can be consumed by front-end (web), mobile or any other third-party system (customer integration etc.)

```json
{
    "data": {
    	# some data here
    },
    "error": null
}
```

### Security

I had no time to implement security in the application. As a consequence, all endpoints are public and insecure.

But still, I would have used Spring Security to provide user authentication.

And also, I would use OAuth2 to authenticate both client applications (web, mobile, etc.) and the user itself.

I could have used JWT to to authenticate requests which can be attached to each HTTP request in the header.


### Scalability

Application does not contain any state. RESTful API itself is stateless. It makes it eligible for scaling and as a result for high availability.

### Tech-stack

Spring-Boot, Java 8, H2, jQuery, Bootstrap

There was nothing mentioned related to persistence layer in the requirements. I decided to use an in-memory database - H2. My intention was to avoid persistence dependency and make it easy for you to demonstrate the application.

___

### Comments

Front-end technologies have evolved enormously in recent years and back-end systems provide an API mostly to be consumed and they don't take any role in the view anymore. The main challenge for me here was to handle both API and the view in the same application. Each part needs to have its own behavior, response model, error handler etc. Apart from that, my other object was to keep the API completely separate from the view, i.e. it should not be aware of any view. 

I managed to handle this issue by using two separate controllers. AssetsController is a REST controller and while it is the main API exposed, the other one, WebController, is responsible to render views. You can observe the same approach in other places. ApiExceptionHandler handles errors in API part while WebExceptionHandler deals with errors of its own.

To represent money, Java Money (JSR-354) is used. For some details, you can check https://github.com/JavaMoney/jsr354-api.
Some open-source libraries (zalando, jadira etc.) are used to persist and serialize Java Money type in order to avoid boiler-plate code. 
 
### Authors
 - Selçuk Karakayalı <skarakayali@gmail.com> 
 
